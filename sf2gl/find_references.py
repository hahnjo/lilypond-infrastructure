#!/usr/bin/env python3

# SPDX-License-Identifier: GPL-3.0-or-later

import json
import os
import re
import sys

json_file = sys.argv[1]
with open(json_file, 'r') as f:
    issues = json.load(f)

tickets = issues['tickets']

def get_ticket_num(ticket):
    return ticket['ticket_num']

def is_ticket(num):
    return 0 < num and num < 6000

def process_referenced(current, referenced):
    if not is_ticket(referenced):
        return

    print('Issue #{current} references #{referenced}{error}'.format(
        current=current,
        referenced=referenced,
        error=' - ERROR' if referenced >= current else ''
    ))

tickets.sort(key=get_ticket_num)

issue1 = re.compile('[Ii]ssue #?(\d+)')
issue2 = re.compile('#(\d+)[ \.)]')

for t in tickets:
    current = get_ticket_num(t)
    description = t['description']
    referenced = set()

    for r in issue1.findall(description):
        if r in referenced:
            continue
        referenced.add(r)
        process_referenced(current, int(r))

    for r in issue2.findall(description):
        if r in referenced:
            continue
        referenced.add(r)
        process_referenced(current, int(r))
