<?php

// SPDX-License-Identifier: GPL-3.0-or-later

@include('config.php');

if (!defined('WEBHOOK_TOKEN')) {
  header('HTTP/1.1 500 Internal Server Error');
  echo 'WEBHOOK_TOKEN not defined!';
  exit(1);
} else if (!defined('GITLAB_API')) {
  define('GITLAB_API', 'https://gitlab.com/api/v4');
} else if (!defined('API_TOKEN')) {
  header('HTTP/1.1 500 Internal Server Error');
  echo 'API_TOKEN not defined!';
  exit(1);
}

if (!isset($_SERVER['HTTP_X_GITLAB_TOKEN']) ||
    $_SERVER['HTTP_X_GITLAB_TOKEN'] != WEBHOOK_TOKEN) {
  header('HTTP/1.1 403 Forbidden');
  return;
}

$stdin = file_get_contents('php://input');
$data = json_decode($stdin, true);
if ($data === null || $data['object_kind'] !== 'merge_request') {
  header('HTTP/1.1 400 Bad Request');
  return;
}

function no_change($message) {
  header('HTTP/1.1 202 Accepted');
  echo $message;
  exit(0);
}

function api_url_project($project) {
  $url = GITLAB_API;
  $url .= '/projects/' . $project['id'];
  return $url;
}

function api_url_mr($project, $mr) {
  $url = GITLAB_API;
  $url .= '/projects/' . $project['id'];
  $url .= '/merge_requests/' . $mr['iid'];
  return $url;
}

function remove_context($diffs) {
  foreach ($diffs as &$entry) {
    if (!array_key_exists('diff', $entry)) {
      continue;
    }

    // Look for context lines (starting with @@) and remove them.
    // The '/m' modifier is PCRE_MULTILINE and makes '^' and '$' match
    // at the beginning and end of each line.
    $entry['diff'] = preg_replace('/^@@ .*$/m', '', $entry['diff']);
  }

  return $diffs;
}

function compare_diff_versions($project, $mr, $old, $new) {
  $versions_url = api_url_mr($project, $mr) . '/versions';

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_HTTPHEADER, [ 'Private-Token: ' . API_TOKEN ]);
  // Set a timeout of 3 seconds because the webhook needs to finish in
  // at most 10 seconds.
  curl_setopt($ch, CURLOPT_TIMEOUT, 3);

  // Get all versions.
  curl_setopt($ch, CURLOPT_URL, $versions_url);
  $response_versions = curl_exec($ch);
  $errno = curl_errno($ch);
  $code = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);
  if ($errno !== 0 || $code !== 200) {
    curl_close($ch);
    return -1;
  }

  // Find base for old and new revision.
  $versions = json_decode($response_versions, true);
  $base_new = null;
  $base_old = null;
  foreach ($versions as $v) {
    $commit = $v['head_commit_sha'];
    $base = $v['base_commit_sha'];
    if ($commit === $old) {
      $base_old = $base;
    } else if ($commit === $new) {
      $base_new = $base;
    }
  }
  if ($base_old === null || $base_new === null) {
    curl_close($ch);
    return -1;
  }

  $compare_url = api_url_project($project) . '/repository/compare';
  // Get diff of old version.
  $url = $compare_url . '?from=' . $base_old;
  $url .= '&to=' . $old;
  curl_setopt($ch, CURLOPT_URL, $url);

  $response_old = curl_exec($ch);
  $errno = curl_errno($ch);
  $code = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);
  if ($errno !== 0 || $code !== 200) {
    curl_close($ch);
    return -1;
  }

  // Get diff of new version.
  $url = $compare_url . '?from=' . $base_new;
  $url .= '&to=' . $new;
  curl_setopt($ch, CURLOPT_URL, $url);

  $response_new = curl_exec($ch);
  $errno = curl_errno($ch);
  $code = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);
  if ($errno !== 0 || $code !== 200) {
    curl_close($ch);
    return -1;
  }

  // Done with sending requests.
  curl_close($ch);

  $response_old = json_decode($response_old, true);
  $response_new = json_decode($response_new, true);

  $diff_old = remove_context($response_old['diffs']);
  $diff_new = remove_context($response_new['diffs']);

  return $diff_old === $diff_new;
}

function send_note($project, $mr, $body) {
  $url = api_url_mr($project, $mr) . '/notes';

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_HTTPHEADER, [ 'Private-Token: ' . API_TOKEN ]);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, [ 'body' => $body ]);

  $response = curl_exec($ch);
  $errno = curl_errno($ch);
  $code = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);
  curl_close($ch);

  if ($errno != 0 || intdiv($code, 100) !== 2) {
    header('HTTP/1.1 500 Internal Server Error');
    echo 'errno: ' . $errno . "\n";
    echo 'HTTP status code: ' . $code . "\n";
  }

  echo $response;
  exit(0);
}

$project = $data['project'];
$mr = $data['object_attributes'];
if (!isset($mr['action'])) {
  no_change('No action given, probably a test.');
}

$action = $mr['action'];
function get_label_title($label) { return $label['title']; }
$labels = array_map('get_label_title', $data['labels']);

$update_rev = ($action == 'update' && isset($mr['oldrev']));
if ($action === 'open' || $action === 'reopen' || $update_rev) {
  if (in_array('Patch::new', $labels)) {
    no_change('Merge Request already has label Patch::new.');
  }
  $body = '';
  if ($update_rev) {
    $old = $mr['oldrev'];
    $new = $mr['last_commit']['id'];

    $equal = compare_diff_versions($project, $mr, $old, $new);
    if ($equal === -1) {
      $body .= 'Failed to get diff for commits ' . $old . ' and ' . $new;
      $body .= ', resetting patch status.' . "\n";
    } else if ($equal) {
      $body .= 'Equal diff for commits ' . $old . ' and ' . $new;
      $body .= ', not resetting patch status.';
      send_note($project, $mr, $body);
    } else {
      echo 'Diff is not empty, proceeding as usual!' . "\n";
    }
  }
  $body .= '/label ~"Patch::new"';
  send_note($project, $mr, $body);
} else if ($action === 'close' || $action === 'merge') {
  foreach ($labels as $label) {
    if (substr($label, 0, 7) === 'Patch::') {
      send_note($project, $mr, '/unlabel ~"' . $label . '"');
      break;
    }
  }
}

no_change('Nothing to do!');
