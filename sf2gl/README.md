LilyPond Issues Migration
=========================

The script `sf2gl.py` is heavily influenced by https://github.com/nijel/sf2gh, but written from scratch to call the GitLab API.

Migration Plan
--------------

1. Create a **private** repository (this will avoid Spam checking for new issues!) and "Disable email notifications" (Settings > General).
2. Mirror repository from Savannah:
   * `$ git clone --bare git://git.savannah.gnu.org/lilypond.git`
   * `$ git push --mirror https://gitlab.com/lilypond/lilypond.git`
3. Disable write access to SourceForge and download an export dump, including attachments.
4. Run `sf2gl.py` with the dump from SourceForge and an API token.
5. Make project public and enable email notifications (Settings > General).
6. Export migrated project before doing any manual changes.
7. Optional: Correct broken references (see `find_references.py`)
