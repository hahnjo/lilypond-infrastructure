#!/usr/bin/env python3

# SPDX-License-Identifier: GPL-3.0-or-later

import argparse
from datetime import datetime
import sys
import requests
import urllib.parse

GITLAB = 'https://gitlab.com/'

# date.fromisoformat was only introduced in Python 3.7, use the old way.
def parse_date(s):
    return datetime.strptime(s, '%Y-%m-%d').date()
def parse_timestamp(s):
    return datetime.strptime(s, '%Y-%m-%dT%H:%M:%S.%f%z')

parser = argparse.ArgumentParser(
    description='Information about CI jobs',
)
parser.add_argument(
    '--repo',
    help='Repository at ' + GITLAB,
    default='lilypond/lilypond'
)
parser.add_argument(
    '--token',
    help=(
        'GitLab token, get it at ' +
        'https://gitlab.com/profile/personal_access_tokens'
    ),
    required=True,
)
parser.add_argument(
    '--start',
    help='Start date (inclusive, format: YYYY-MM-DDD)',
    type=parse_date,
)
parser.add_argument(
    '--end',
    help='End date (exclusive, format: YYYY-MM-DDD)',
    type=parse_date,
)

args = parser.parse_args(sys.argv[1:])

# Create a session to speed up multiple requests.
s = requests.Session()
# Supply token
s.headers = {'Private-Token': args.token}

api_url = GITLAB + 'api/v4/projects/' + urllib.parse.quote_plus(args.repo)

# Load all jobs after the start date.
jobs = []
jobs_url = api_url + '/jobs?per_page=100'
while jobs_url:
    response = s.get(jobs_url)
    jobs += response.json()

    # The jobs are ordered by ID, so also by date. Stop loading if the last job
    # is already before the start date.
    if args.start is not None:
        last_job = parse_timestamp(jobs[-1]['created_at'])
        if last_job.date() < args.start:
            break

    # Determine 'next' link.
    if not 'next' in response.links:
        break
    jobs_url = response.links['next']['url']

# Filter the jobs based on their time.
if args.start is not None or args.end is not None:
    def between_start_and_end(j):
        d = parse_timestamp(j['created_at']).date()
        if args.start is not None and d < args.start:
            return False
        if args.end is not None and args.end <= d:
            return False
        return True

    jobs = filter(between_start_and_end, jobs)

def init_runner(runner):
    runner['jobs'] = 0
    runner['duration'] = 0.0

# Aggregate per runner.
runners = {}
unknown_runners = {}
init_runner(unknown_runners)

for j in jobs:
    if j['duration'] is None:
        continue

    if j['runner'] is None:
        runner = unknown_runners
    else:
        # Insert if new runner.
        runner_id = j['runner']['id']
        runner = runners.get(runner_id)
        if runner is None:
            runner = j['runner'].copy()
            init_runner(runner)
            runners[runner_id] = runner

    runner['jobs'] += 1
    runner['duration'] += j['duration']

# Distinguish private and shared runners.
private_runners = []
shared_runners = []
for r in runners.values():
    if r['is_shared']:
        shared_runners.append(r)
    else:
        private_runners.append(r)

def minutes_seconds(duration):
    return (
        int(duration // 60),
        int(duration % 60),
    )

def print_runners(runners):
    runners = sorted(runners, key=lambda r: r['jobs'], reverse=True)

    jobs = 0
    duration = 0.0
    for r in runners:
        jobs += r['jobs']
        duration += r['duration']

        minutes, seconds = minutes_seconds(r['duration'])
        print(' * {runner}: {jobs} jobs in {minutes}m{seconds:02}'.format(
            runner=r['description'],
            jobs=r['jobs'],
            minutes=minutes,
            seconds=seconds,
        ))

    minutes, seconds = minutes_seconds(duration)
    print('Total: {jobs} jobs in {minutes}m'.format(
        jobs=jobs,
        minutes=minutes,
    ))

print('CI Statistics for ' + args.repo)
if args.start is not None and args.end is not None:
    print('(from {start} to {end})'.format(start=args.start, end=args.end))
elif args.start is not None:
    print('(since {start})'.format(start=args.start))
elif args.end is not None:
    print('(until {end})'.format(end=args.end))
print('')

print('Private runners:')
print_runners(private_runners)

print('\nShared runners:')
print_runners(shared_runners)

if unknown_runners['jobs'] > 0:
    minutes, seconds = minutes_seconds(unknown_runners['duration'])
    print('\nUnknown runners: {jobs} jobs in {minutes}m{seconds:02}'.format(
        jobs=unknown_runners['jobs'],
        minutes=minutes,
        seconds=seconds,
    ))
